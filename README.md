# aGosard
Repo for different projects in Go made by @arosard

## List of current projects

| Project Name | Status   | Working   | webServer integration | Description                                                     |
|--------------|----------|-----------|-----------------------|-----------------------------------------------------------------|
| goRequete    | Inactive | Yes       | No                    | Program to automate http request to test website load balancing |
| codeGovern   | Active   | Yes       | Yes                   | Program to help maintain applications and check for updates     |

## To run the project locally
Without logs :
```shell
make run
```
With all logs :
```shell
make debug
```
or custom :
```bash
go run main.go [-debug N [-debuglocation LOCATION] ] [-port PORTNUMBER -hostname HOSTNAME]
```
## How options work 
There are several options for development and deployment purposes only at the moment :
- `-debug` : to specify the log level needed : 
  - `0` (default) : No logs 
  - `1` : Public functions calls
  - `2` : + Private functions calls
  - `3` : + Function execution logs
- `-debuglocation` : to specify where the debug parameter should be applied
  - ` ` (default) : nowhere
  - `codeGovvern` : only for the codeGovern functions
  - `main` : for the webserver only
  - `all` : everywhere
- `hostname` : name of the host for the webServer
  - `localhost` (default)
- `port` : port number through which the webServer will be accessible
  - `8000` (default)

The webServer options can be modified in the `makefile` to avoid typing the whole `go` command.
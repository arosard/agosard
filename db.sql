DROP DATABASE IF EXISTS agosard ;
CREATE DATABASE agosard;
CREATE TABLE codegovern(
    appID INTEGER PRIMARY KEY,
    appName TEXT,
    codeOwner TEXT NOT NULL,
    repository TEXT NOT NULL,
    actualRelease TEXT,
    latestRelease TEXT,
    latestReleaseNotes TEXT,
    description TEXT
);
# syntax=docker/dockerfile:1

FROM golang:1.20.2

WORKDIR /app
ENV GO111MODULE=on
ADD . .
RUN go mod download
RUN go build -v -o main
RUN chmod +x main
EXPOSE 8000

CMD [ "./main" ]

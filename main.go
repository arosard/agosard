package main

import (
	"codeGovern"
	"flag"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strconv"
)

// DebugLevel is used to define the verbosity of the debug logs
//
// 0 - No debug logs
//
// 1 - Public function calls only
//
// 2 - Adds private function calls
//
// 3 - Adds information about function execution
//
// Less than 0 will be considered as 0.
// More than 3 will be considered as 3.
var DebugLevel int

// Initial Template Caching to avoid parsing a template each time we render
var templates = make(map[string]*template.Template)

// To check if the request is meaningful
var validPath = regexp.MustCompile("^/((edit|view|refresh|save|delete)/([0-9]+)|home/|save/|add/|refresh/|templates/|not_found/)$")

// Function that loads all the templates once
func initializeTemplates() {
	templates["view"] = template.Must(template.ParseFiles("templates/view.html", "templates/general/base.html"))
	templates["edit"] = template.Must(template.ParseFiles("templates/edit.html", "templates/general/base.html"))
	templates["home"] = template.Must(template.ParseFiles("templates/home.html", "templates/general/base.html"))
	templates["add"] = template.Must(template.ParseFiles("templates/add.html", "templates/general/base.html"))
	templates["pageNotFound"] = template.Must(template.ParseFiles("templates/pageNotFound.html", "templates/general/base.html"))
}

// Function that renders the template with the Data's content
func renderDataTemplate(w http.ResponseWriter, tmpl string, d *codeGovern.Data) {
	if DebugLevel >= 3 {
		log.Println("Call to renderDataTemplate : rendering template " + tmpl)
	}
	err := templates[tmpl].ExecuteTemplate(w, "base", d)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Renders the template of the homepage
func renderHomeTemplate(w http.ResponseWriter, tmpl string, dArr []*codeGovern.Data) {
	if DebugLevel >= 1 {
		log.Println("renderHomeTemplate called")
	}
	err := templates[tmpl].ExecuteTemplate(w, "base", dArr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Function that performs automatic check of the path and then redirect to the handler function
func makeHandler(fn func(http.ResponseWriter, *http.Request, int)) http.HandlerFunc {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("Checking the request with function makeHandler")
	}
	return func(w http.ResponseWriter, r *http.Request) {
		if DebugLevel >= 3 {
			log.Println(r.URL.Path)
		}
		m := validPath.FindStringSubmatch(r.URL.Path)
		if DebugLevel >= 3 {
			log.Println(m)
		}
		if m == nil {
			http.NotFound(w, r)
			return
		}
		// third argument corresponds to an id
		if m[3] != "" {
			id, err := strconv.Atoi(m[3])
			if err != nil {
				log.Fatalf("Could not convert index "+m[3]+" to int : %v\n", err)
			} else {
				fn(w, r, id)
				return
			}
		}
		fn(w, r, -1)
	}
}

// Handler function that gives the view of the home page of the codeGovern app
func homeHandler(w http.ResponseWriter, _ *http.Request, _ int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("Home handler called")
	}
	renderHomeTemplate(w, "home", codeGovern.ReadAllData())
}

// addHandler function that gives the view of the home page of the codeGovern app
func addHandler(w http.ResponseWriter, _ *http.Request, _ int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("Home handler called")
	}
	renderHomeTemplate(w, "add", codeGovern.ReadAllData())
}

// Handler function that gives the view of an app
func viewHandler(w http.ResponseWriter, r *http.Request, id int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("View handler called on page " + strconv.Itoa(id))
	}
	p, err := codeGovern.ReadDataFromID(id)
	if err != nil {
		http.Redirect(w, r, "/edit/"+strconv.Itoa(id), http.StatusNotFound)
		return
	}
	renderDataTemplate(w, "view", p)
}

// Handler function that gives the edit view of an app
func editHandler(w http.ResponseWriter, r *http.Request, id int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("editHandler called on page with id : " + strconv.Itoa(id))
	}
	d, err := codeGovern.ReadDataFromID(id)
	if err != nil { // If page does not exist already, creates new page
		if DebugLevel >= 3 {
			log.Printf("Page with id %d not found.", id)
		}
		http.Redirect(w, r, "/not_found/", http.StatusNotFound)
		return
	}
	renderDataTemplate(w, "edit", d)
}

func pageNotFoundHandler(w http.ResponseWriter, _ *http.Request, _ int) {
	if DebugLevel >= 1 {
		log.Println("pageNotFoundHandler called")
	}
	renderDataTemplate(w, "pageNotFound", nil)
}

// Handler function that saves all changes made to an app
func saveHandler(w http.ResponseWriter, r *http.Request, id int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("Save handler called")
	}
	newData, err := codeGovern.ReadDataFromID(id)
	if DebugLevel >= 3 {
		log.Println("Forms data :")
		log.Println("AppName : " + r.FormValue("AppName"))
		log.Println("CodeOwner : " + r.FormValue("CodeOwner"))
		log.Println("Repository : " + r.FormValue("Repository"))
		log.Println("ActualRelease : " + r.FormValue("ActualRelease"))
		log.Println("Description : " + r.FormValue("Description"))
	}
	if err == nil {
		if DebugLevel >= 3 {
			log.Printf("Data found at id %d\n", id)
		}
		(*newData).AppName = r.FormValue("AppName")
		(*newData).CodeOwner = r.FormValue("CodeOwner")
		(*newData).Repository = r.FormValue("Repository")
		(*newData).ActualRelease = r.FormValue("ActualRelease")
		(*newData).Description = r.FormValue("Description")
		err = codeGovern.UpdateDatabase(newData)
	} else {
		if DebugLevel >= 3 {
			log.Printf("Data not found at id %d\n", id)
			log.Printf("Error : %v\n", err)
		}
		appName := r.FormValue("AppName")
		codeOwner := r.FormValue("CodeOwner")
		repository := r.FormValue("Repository")
		actualRelease := r.FormValue("ActualRelease")
		description := r.FormValue("Description")
		newData = &codeGovern.Data{
			AppName:       appName,
			CodeOwner:     codeOwner,
			Repository:    repository,
			ActualRelease: actualRelease,
			Description:   description,
		}
		err = codeGovern.AddDataToDatabase(newData)
	}
	if err != nil {
		log.Printf("Error while saving in database : %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+strconv.Itoa(newData.AppID()), http.StatusFound)
}

// Handler function that refreshes all the fields related to the latest release of an app specified by id
func refreshHandler(w http.ResponseWriter, r *http.Request, id int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("refreshHandler called")
	}
	if id >= 0 {
		data, err := codeGovern.ReadDataFromID(id)
		if err != nil {
			if DebugLevel >= 3 {
				log.Printf("Error during data extraction : %v\n", err)
			}
			http.Redirect(w, r, "/home/", http.StatusInternalServerError)
		}
		codeGovern.UpdateRelease(data)
		err = codeGovern.UpdateDatabase(data)
		if err != nil {
			if DebugLevel >= 3 {
				log.Printf("Error during database update : %v\n", err)
			}
			http.Redirect(w, r, "/home/", http.StatusInternalServerError)
		}
		http.Redirect(w, r, "/view/"+strconv.Itoa(id), http.StatusFound)
	} else {
		err := codeGovern.UpdateAllReleasesInDatabase()
		if err != nil {
			if DebugLevel >= 3 {
				log.Printf("The refresh action failed : %v\n", err)
			}
			http.Redirect(w, r, "/home/", http.StatusInternalServerError)
		}
	}
	http.Redirect(w, r, "/home/", http.StatusFound)
}

// Handler function to delete an existing element
func deleteHandler(w http.ResponseWriter, r *http.Request, id int) {
	if DebugLevel >= 1 { // Logs pour le debug
		log.Println("DeleteHandler called")
	}
	_, err := codeGovern.ReadDataFromID(id)
	if err != nil {
		http.Redirect(w, r, "/not_found/", http.StatusNotFound)
		return
	}
	err = codeGovern.DeleteDataFromID(id)
	if err != nil {

	}
	http.Redirect(w, r, "/home/", http.StatusFound)
}

// Handler function that refreshes templates, useful when editing them
func templatesHandler(w http.ResponseWriter, r *http.Request, _ int) {
	initializeTemplates()
	http.Redirect(w, r, "/home/", http.StatusFound)
}

// First argument specified should be host-port and second should be host-server's name
func main() {
	log.Println("Processing flags")
	var debugLevel, port int
	var host, debugLocation string
	flag.IntVar(&debugLevel, "debug", 0, "Used to specify how much of the debug logs are to be displayed.")
	flag.IntVar(&port, "port", 8000, "Port to map the output of the webserver.")
	flag.StringVar(&host, "hostname", "", "Name of the host of the webserver.")
	flag.StringVar(&debugLocation, "debugLocation", "main", "Used to specify where the debug parameter applies")
	flag.Parse()
	switch debugLocation {
	case "main":
		DebugLevel = debugLevel
		codeGovern.DebugLevel = 0
	case "codeGovern":
		DebugLevel = 0
		codeGovern.DebugLevel = debugLevel
	case "all":
		DebugLevel = debugLevel
		codeGovern.DebugLevel = debugLevel
	default:
		DebugLevel = 0
		codeGovern.DebugLevel = 0
	}
	if host != "localhost" {
		log.Println("Host is not \"localhost\" hence skipping .env loading")
	} else {
		log.Println("Initializing env variables from the .env file")
		codeGovern.InitializeEnvVariables()
	}

	log.Println("Initializing database connection from env variables")
	codeGovern.InitializeDatabase()
	log.Println("Initializing html templates")
	// Used to load css files
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	initializeTemplates()
	log.Println("Setting up handler functions")
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	http.HandleFunc("/refresh/", makeHandler(refreshHandler))
	http.HandleFunc("/home/", makeHandler(homeHandler))
	http.HandleFunc("/templates/", makeHandler(templatesHandler))
	http.HandleFunc("/add/", makeHandler(addHandler))
	http.HandleFunc("/delete/", makeHandler(deleteHandler))
	http.HandleFunc("/not_found/", makeHandler(pageNotFoundHandler))
	address := host + ":" + strconv.Itoa(port)
	log.Println("Listening on " + address)
	log.Fatalln(http.ListenAndServe(address, nil))
	// ":8000" : firewall will ask for permission locally
	// "localhost:8000" : doesn't "work" with docker
}

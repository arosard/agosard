# goRequete
## Summary
goRequete is a Go script to execute multiple http GET request to a specified website.
It returns the number of requests which failed and succeeded and the corresponding percentage.
## Usage
To get help :
```go
go run goRequete.go
```

To execute requests
```go
go run goRequete.go <nbRequest> <target>
```
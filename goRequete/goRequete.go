package goRequete

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	size := len(os.Args)
	// Errors on number of inputs
	switch size {
	case 1:
		fmt.Println("Use of function : nbRequests target\nnbRequests : number of GET requests to make\ntarget : target of the requests")
		os.Exit(0)
	case 3:
		// Not an error then
	default:
		fmt.Println("Invalid number of arguments !")
		os.Exit(0)
	}
	nbRequests, err := strconv.Atoi(os.Args[1])
	target := os.Args[2]
	if err != nil {
		fmt.Println("Error : Argument 1 should be a number ")
		os.Exit(1)
	}
	worked := 0
	errors := 0
	ch := make(chan int)
	fmt.Print("Starting requests to " + target)
	start := time.Now()
	for i := 0; i < nbRequests; i++ {
		go makeRequest(target, ch)
	}
	for i := 0; i < nbRequests; i++ {
		out := <-ch
		worked += 1 - out
		errors += out
	}
	total := worked + errors
	elapsed := time.Since(start)
	fmt.Printf("\nTotal : %d\n", total)
	fmt.Printf("Worked : %d (%d%%)\n", worked, worked*100/total)
	fmt.Printf("Failed : %d (%d%%)\n", errors, errors*100/total)
	fmt.Printf("Executed in %s\n", elapsed)
}

func makeRequest(target string, ch chan<- int) {
	_, err := http.Get(target)
	if err != nil {
		ch <- 1
	} else {
		ch <- 0
	}
}

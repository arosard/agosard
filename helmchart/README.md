## Helm 
Chart to deploy agosard to Kubernetes
## Values
| name            | values                          | Description                                     |
|-----------------|---------------------------------|-------------------------------------------------|
| ingress.enabled | `true` or `false`               | Specifies if an ingress will be created         |
| ingress.host    | string                          | Website's name where it will be accessible from |
| ingress.path    | String                          | ?                                               | 
| ingres.pathType | `ImplementationSpecific` or `?` | ?                                               |
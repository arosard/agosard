CONTAINERPORT = 8000
HOSTPORT = 8000
IMAGE = agosard
VERSION = v1.0.0

run:
	go run main.go -port ${HOSTPORT} -hostname localhost -debug=0
debug:
	go run main.go -port ${HOSTPORT} -hostname localhost -debug=3 -debugLocation=all
docker-image:
	docker build . -t ${IMAGE}:${VERSION}
docker-run:
	docker run -d -p ${HOSTPORT}:${CONTAINERPORT} --name ${IMAGE} ${IMAGE}:${VERSION}

package codeGovern

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/go-github/v50/github"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/joho/godotenv"
	"html/template"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// DebugLevel is used to define the verbosity of the debug logs
//
// 0 - No debug logs
//
// 1 - Public function calls only
//
// 2 - Adds private function calls
//
// 3 - Adds information about function execution
//
// Less than 0 will be considered as 0.
// More than 3 will be considered as 3.
var DebugLevel int

// clientGitHub is the client used to make request to the GitHub API
var clientGitHub *github.Client

// Data corresponds to the data stored in the database.
type Data struct {
	// The data's primary key in the database, must be an integer
	appID int
	// The data's referred name
	AppName string
	// The GitHub account which owns the app's repository
	CodeOwner string
	// The app's GitHub repository
	Repository string
	// The release in-use
	ActualRelease string
	// The latest release of the app available in the repository
	LatestRelease string
	// The latest release's notes available in the repository in HTML format
	LatestReleaseNotes template.HTML
	// A description of the app
	Description string
}

// AppID returns the Data.appID attribute
func (d *Data) AppID() int {
	return d.appID
}

// Variable that stores the biggest id that is actually used in the database.
//
// It is used to add new elements to the database
var maxID int

// Global database used in the package.
//
// Postgres by default but any database with go drivers that supports the "database/sql" integration can be used with minor modifications.
var db *sql.DB

// Table where the corresponding data is stored.
// Value is set when InitializeDatabase is called
var tableName string

// InitializeEnvVariables extracts the environment variables stored in the .env file.
func InitializeEnvVariables() {
	if DebugLevel >= 1 {
		log.Println("InitializeEnvVariables called")
	}
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Unable to locate .env file: %v\n", err)
	}
	if DebugLevel >= 3 {
		log.Println("Env variables initialized (values not checked yet)")
	}
}

func createTable() {
	if DebugLevel >= 2 {
		log.Println("createTable called")
	}
	_, err := db.Exec("CREATE TABLE codegovern( " +
		"appID INTEGER PRIMARY KEY, " +
		"appName TEXT, " +
		"codeOwner TEXT NOT NULL, " +
		"repository TEXT NOT NULL, " +
		"actualRelease TEXT, " +
		"latestRelease TEXT, " +
		"latestReleaseNotes TEXT, " +
		"description TEXT)")
	if err != nil {
		log.Fatalf("Error creating table : %v\n", err)
	}
}

// InitializeDatabase opens a database.
//
// The parameters are given by the environment variables in the .env file.
func InitializeDatabase() {
	tableName = os.Getenv("TABLENAME")
	if DebugLevel >= 1 {
		log.Println("InitializeDatabase called")
	}
	var err error
	db, err = sql.Open("pgx", os.Getenv("DATABASEURL"))
	if err != nil {
		log.Fatalf("Unable to create connection pool: %v\n", err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatalf("Database connection failed: %v\n", err)
	}
	if DebugLevel >= 3 {
		log.Println("Database connection Succeeded")
	}
	// Checks if the database is not empty
	query := "SELECT count(*) FROM " + tableName
	err = db.QueryRow(query).Scan(&maxID)
	if err != nil {
		log.Printf("Error querying the database : %v\nTrying to create table\n", err)
		createTable()
	}
	if maxID > 0 { // Database is not empty
		query = "SELECT max(appID) FROM " + tableName
		err = db.QueryRow(query).Scan(&maxID)
		if err != nil {
			log.Fatalf("Error getting the maximum appID : %v\n", err)
		}
	}
	if DebugLevel >= 3 {
		log.Println(maxID)
	}
	clientGitHub = github.NewClient(nil)
}

// updateGithubRelease checks the Latest release of the GitHub Repository
func updateGithubRelease(data *Data) (string, string) {
	if DebugLevel >= 2 {
		log.Println("Getting update from github.com/" + data.CodeOwner + "/" + data.Repository)
	}

	rs, _, err := clientGitHub.Repositories.GetLatestRelease(context.Background(), data.CodeOwner, data.Repository)
	if err != nil {
		if DebugLevel >= 2 {
			log.Println("Get update from repository failed")
		}
		return "Not found", "Not found"
	} else {
		log.Println("Get update from repository succeeded")
		return rs.GetTagName(), rs.GetBody()
	}
}

// updateBitnamiRelease checks the latest release of the Bitnami charts repo Repository
func updateBitnamiRelease(data *Data) (string, string) {
	if DebugLevel >= 2 {
		log.Println("Getting update from github.com/bitnami/charts/tree/main/bitnami/" + data.AppName)
	}
	repoCommits, _, err := clientGitHub.Repositories.ListCommits(context.Background(), "bitnami", "charts", &github.CommitsListOptions{Path: "bitnami/" + data.AppName + "/"})
	if err != nil {
		if DebugLevel >= 3 {
			log.Printf("Error listing commits : %v\n", err)
		}
		return "Not found", "Not found"
	}
	var latestCommit *github.RepositoryCommit
	for _, commit := range repoCommits {
		if strings.Contains(commit.Commit.GetMessage(), "[bitnami/"+data.AppName+"] Release") {
			if DebugLevel >= 3 {
				log.Println("Release found among commits")
				log.Println(commit.Commit.GetMessage())
			}
			latestCommit = commit
			break
		}
	}
	if latestCommit == nil {
		if DebugLevel >= 3 {
			log.Println("No commit found that matches the expression")
		}
		return "Not found", "Not found"
	}
	rex := "\\[bitnami/" + data.AppName + "] Release ([0-9]+(\\.[0-9]+)*)"
	if DebugLevel >= 3 {
		log.Println("Regular expression to extract release : " + rex)
	}
	compiledRex := regexp.MustCompile(rex)
	foundMatches := compiledRex.FindAllString(latestCommit.Commit.GetMessage(), 1)
	if DebugLevel >= 3 {
		log.Println(foundMatches)
	}
	if len(foundMatches) == 0 {
		if DebugLevel >= 3 {
			log.Println("Could not extract version from commit")
		}
		return "Not found", "Not found"
	} else {
		if DebugLevel >= 3 {
			log.Println("Line with release found : " + foundMatches[0])
		}
		latestReleaseFound := strings.ReplaceAll(foundMatches[0], "[bitnami/"+data.AppName+"] Release ", "")
		if DebugLevel >= 3 {
			log.Printf("LatestRelease : %s\nLatestReleaseNotes : %s\n", latestReleaseFound, latestCommit.Commit.GetMessage())
		}
		return latestReleaseFound, latestCommit.Commit.GetMessage()
	}

}

// UpdateRelease performs an update of the data's LatestRelease and LatestReleaseNotes attributes.
// If the latestRelease cannot be processed "Not found" will be returned for both attribute.
//
// Depending on the codeOwner attribute :
//
// - If it is "bitnami" : the update function for bitnami's charts repository will be called
//
// - Else : the default GitHub update function will be called
func UpdateRelease(data *Data) {
	if DebugLevel >= 1 {
		log.Println("Function UpdateRelease called")
	}
	var latestRelease, latestReleaseNotes string
	if data.CodeOwner == "bitnami" {
		latestRelease, latestReleaseNotes = updateBitnamiRelease(data)
	} else {
		latestRelease, latestReleaseNotes = updateGithubRelease(data)
	}
	renderedNotes, _, err := clientGitHub.Markdown(context.Background(), latestReleaseNotes, nil)
	if err != nil {
		fmt.Printf("Markdown error : %v\n", err)
		data.LatestReleaseNotes = "Rendering error"
	} else {
		data.LatestReleaseNotes = template.HTML(renderedNotes)
	}
	data.LatestRelease = latestRelease
	err = UpdateDatabase(data)
	if err != nil {
		log.Printf("Error updating release in database : %v\n", err)
	}
}

// AddDataToDatabase adds a new row in the database
//
// An error is returned if the query fails
func AddDataToDatabase(d *Data) error {
	maxID += 1
	request := fmt.Sprintf("INSERT INTO %s VALUES (%d,'%s','%s','%s','%s','%s','%s','%s')", tableName, maxID, d.AppName, d.CodeOwner, d.Repository, d.ActualRelease, d.LatestRelease, d.LatestReleaseNotes, d.Description)
	_, err := db.Exec(request)
	if err == nil {
		d.appID = maxID
	}
	return err
}

// ReadDataFromID returns the data stored in the row with the specified id.
//
// An error is returned if the query fails or if it returns an empty result.
func ReadDataFromID(id int) (*Data, error) {
	var result Data
	query := "SELECT * FROM " + tableName + " WHERE Appid = " + strconv.Itoa(id)
	err := db.QueryRow(query).Scan(&result.appID, &result.AppName, &result.CodeOwner, &result.Repository, &result.ActualRelease, &result.LatestRelease, &result.LatestReleaseNotes, &result.Description)
	if err != nil {
		if err == sql.ErrNoRows {
			return &result, err
		} else {
			log.Fatalf("Problem with SELECT query : %v\n", err)
			return &result, err
		}

	} else {
		return &result, nil
	}
}

// ReadAllData returns the whole content of the table.
func ReadAllData() []*Data {
	rows, err := db.Query("SELECT AppId FROM " + tableName)
	if err != nil {
		log.Fatal(err)
	}
	var numberRows int
	err = db.QueryRow("SELECT count(*) FROM " + tableName).Scan(&numberRows)
	if err != nil {
		log.Fatal(err)
	}
	var dSlice = make([]*Data, numberRows)
	var id int
	i := 0
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		dSlice[i], err = ReadDataFromID(id)
		if err != nil {
			log.Fatal(err)
		}
		i++
	}
	return dSlice
}

// DeleteDataFromID deletes the row specified by its id.
//
// An error is returned if the query fails.
func DeleteDataFromID(id int) error {
	request := "DELETE FROM " + tableName + " WHERE Appid = " + strconv.Itoa(id)
	_, err := db.Exec(request)
	return err
}

// DeleteAllData deletes the whole content of the table.
//
// An error is returned if the query fails.
func DeleteAllData() error {
	request := "DELETE FROM " + tableName
	_, err := db.Exec(request)
	return err
}

func cleanString(a string) string {
	return strings.ReplaceAll(a, "'", "''")
}

// UpdateDatabase updates the row with the data specified in d.
//
// An error is returned if the row's id is not found in the database or if the query fails.
func UpdateDatabase(d *Data) error {
	_, err := ReadDataFromID(d.appID)
	var request string
	if err != nil {
		return err
	}
	request = fmt.Sprintf("UPDATE %s SET AppName = '%s', CodeOwner = '%s', Repository = '%s', ActualRelease = '%s', LatestRelease = '%s', LatestReleaseNotes = '%s', Description = '%s' WHERE Appid = %d", tableName, d.AppName, d.CodeOwner, d.Repository, d.ActualRelease, d.LatestRelease, cleanString(string(d.LatestReleaseNotes)), d.Description, d.appID)
	_, err = db.Exec(request)
	return err
}

// UpdateAllReleasesInDatabase performs an update of all apps' LatestRelease and LatestReleaseNotes
//
// An error is returned if a query fails.
func UpdateAllReleasesInDatabase() error {
	query := "SELECT AppID FROM " + tableName
	var rows *sql.Rows
	rows, err := db.Query(query)
	if err != nil {
		return err
	}
	var d *Data
	var id int
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		d, err = ReadDataFromID(id)
		if err != nil {
			return err
		}
		UpdateRelease(d)
		err = UpdateDatabase(d)
		if err != nil {
			return err
		}
	}
	return nil
}

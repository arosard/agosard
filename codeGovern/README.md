# codeGovern
## Summary
codeGovern is a package designed to ease applications maintaining by :
- Automatically checking for new releases
- Grouping release notes in one place
## Usage
It interacts with a database where the data is stored.  
The `Data` struct is an abstraction of the data represented in the database to be used  with a go application
### To run tests locally :
Prior to the test
- A database should be set up on localhost. Currently only Postgresql databases are supported.
- Connection parameters should be provided in the `.env` file.
- The script `database-test/create-test-table.sql` should be run before launching tests.
---
Execute the following command inside this directory :
```shell
go test -v
```

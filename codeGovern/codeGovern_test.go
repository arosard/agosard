package codeGovern

import (
	"database/sql"
	"testing"
)

// Test of the updateGithubRelease function that is supposed to work.
func TestUpdateGithubRelease(t *testing.T) {
	InitializeEnvVariables()
	InitializeDatabase()
	data := &Data{
		CodeOwner:     "joho",
		Repository:    "godotenv",
		LatestRelease: "Not found",
	}
	latestRelease, _ := updateGithubRelease(data)
	if latestRelease == "Not found" {
		t.Errorf("Repository not found when it should be.\n")
	}
}

func TestUpdateBitnamiRelease(t *testing.T) {
	data := &Data{
		CodeOwner:  "bitnami",
		Repository: "charts",
		AppName:    "wordpress",
	}
	latestRelease, _ := updateBitnamiRelease(data)
	if latestRelease == "Not Found" {
		t.Errorf("Release not found when it should be.\n")
	}
}

// Test of the initializeDatabase function that is supposed to work.
func TestInitializeDatabase(t *testing.T) {
	InitializeEnvVariables()
	InitializeDatabase()
	err := db.Ping()
	if err != nil {
		t.Errorf("Database initialization failed : %v\n", err)
	}
}

// Two tests of the initializeDatabase function.
// The first should work.
// The second should fail.
func TestReadDataFromID(t *testing.T) {
	InitializeEnvVariables()
	InitializeDatabase()
	// Test that should work
	dataRef := &Data{
		appID:              1,
		AppName:            "godotenv",
		CodeOwner:          "joho",
		Repository:         "godotenv",
		ActualRelease:      "Not Found",
		LatestRelease:      "Not Found",
		LatestReleaseNotes: "Not Found",
		Description:        "Go module to load environment variables from a file",
	}
	data, err := ReadDataFromID(1)
	if err != nil {
		t.Errorf("Error while Readting data : %v\n", err)
	}
	checkMatch(data, dataRef, t)

	// Test that should fail because of non-existent index
	data, err = ReadDataFromID(5)
	if err != sql.ErrNoRows {
		t.Errorf("Expected ErrorNoRows but got %v\n", err)
	}
}

// Test of the ReadAllData function that should work
func TestReadAllData(t *testing.T) {
	InitializeEnvVariables()
	InitializeDatabase()
	var dataArrayRef = make([]*Data, 3)
	dataArrayRef[0] = &Data{
		AppName:            "Go Github",
		CodeOwner:          "google",
		Repository:         "go-github",
		ActualRelease:      "Not Found",
		LatestRelease:      "Not Found",
		LatestReleaseNotes: "Not Found",
		Description:        "Go module to check for Github updates",
	}
	dataArrayRef[1] = &Data{
		AppName:            "godotenv",
		CodeOwner:          "joho",
		Repository:         "godotenv",
		ActualRelease:      "Not Found",
		LatestRelease:      "Not Found",
		LatestReleaseNotes: "Not Found",
		Description:        "Go module to load environment variables from a file",
	}

	dataArrayRef[2] = &Data{
		AppName:            "Go PSQL Drivers",
		CodeOwner:          "jackc",
		Repository:         "pgx/v5/pgxpool",
		ActualRelease:      "Not Found",
		LatestRelease:      "Not Found",
		LatestReleaseNotes: "Not Found",
		Description:        "Go module to load environment variables from a file",
	}
	dataArray := ReadAllData()
	if len(dataArray) != len(dataArrayRef) {
		t.Fatalf("Different array sizes : reference array is %d, got %d\n", len(dataArrayRef), len(dataArray))
	}
	for i := 0; i < 3; i++ {
		checkMatch(dataArray[i], dataArrayRef[i], t)
	}
}

// Test of the updateDatabase and deleteDataFromID functions that should work.
//
// The first test adds a new row to the database.
// The second test updates that row.
// The third test deletes the added row.
func TestInsertUpdateDeleteDatabase(t *testing.T) {
	InitializeEnvVariables()
	InitializeDatabase()

	// Test inserting new row
	data := &Data{
		AppName:            "Kubernetes",
		CodeOwner:          "kubernetes",
		Repository:         "kubernetes",
		ActualRelease:      "Not found",
		LatestRelease:      "Not found",
		LatestReleaseNotes: "Not found",
		Description:        "Container orchestration software coded in go.",
	}
	err := UpdateDatabase(data)
	if err != nil {
		t.Errorf("Error updating the database : %v\n", err)
	}
	data2, err := ReadDataFromID(4)
	if err != nil {
		t.Errorf("Error while checking inserted row : %v\n", err)
	}
	checkMatch(data2, data, t)

	// Test updating existing row
	data.AppName = "K8s"
	err = UpdateDatabase(data)
	if err != nil {
		t.Errorf("Error updating the database : %v\n", err)
	}
	data2, err = ReadDataFromID(4)
	if err != nil {
		t.Errorf("Error while checking updated row : %v\n", err)
	}
	checkMatch(data2, data, t)

	err = DeleteDataFromID(4)
	if err != nil {
		t.Errorf("Data deletion failed : %v\n", err)
	}
	_, err = ReadDataFromID(4)
	if err != sql.ErrNoRows {
		t.Errorf("Row deletion did not succeed : %v\n", err)
	}
}

// Function to check if two Data struct instances' fields are the same.
//
// If there is a difference, a testing Error message is shown.
func checkMatch(data *Data, dataRef *Data, t *testing.T) {
	if data.AppID() != dataRef.AppID() {
		t.Errorf("Incorrect AppIData.\nExpected %d got %d\n", data.AppID(), dataRef.AppID())
	}
	if data.AppName != dataRef.AppName {
		t.Errorf("Incorrect AppName.\nExpected \"%s\" got \"%s\"\n", dataRef.AppName, data.AppName)
	}
	if data.CodeOwner != dataRef.CodeOwner {
		t.Errorf("Incorrect CodeOwner.\nExpected \"%s\" got \"%s\"\n", dataRef.CodeOwner, data.CodeOwner)
	}
	if data.Repository != dataRef.Repository {
		t.Errorf("Incorrect Repository.\nExpected \"%s\" got \"%s\"\n", dataRef.Repository, data.Repository)
	}
	if data.ActualRelease != dataRef.ActualRelease {
		t.Errorf("Incorrect ActualRelease.\nExpected \"%s\" got \"%s\"\n", dataRef.ActualRelease, data.ActualRelease)
	}
	if data.LatestRelease != dataRef.LatestRelease {
		t.Errorf("Incorrect LatestRelease.\nExpected \"%s\" got \"%s\"\n", dataRef.LatestRelease, data.LatestRelease)
	}
	if data.LatestReleaseNotes != dataRef.LatestReleaseNotes {
		t.Errorf("Incorrect LatestReleaseNotes.\nExpected \"%s\" got \"%s\"\n", dataRef.LatestReleaseNotes, data.LatestReleaseNotes)
	}
	if data.Description != dataRef.Description {
		t.Errorf("Incorrect Description.\nExpected \"%s\" got \"%s\"\n", dataRef.Description, data.Description)
	}
}

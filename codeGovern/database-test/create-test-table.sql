DROP TABLE testPagesCode;
CREATE TABLE testPagesCode (
                           appID INTEGER PRIMARY KEY,
                           appName TEXT,
                           codeOwner TEXT NOT NULL,
                           repository TEXT NOT NULL,
                           actualRelease TEXT,
                           latestRelease TEXT,
                           latestReleaseNotes TEXT,
                           description TEXT
);

INSERT INTO testPagesCode
VALUES
    (0,'Go Github','google','go-github','Not Found','Not Found','Not Found','Go module to check for Github updates'),
    (1,'godotenv','joho','godotenv','Not Found','Not Found','Not Found','Go module to load environment variables from a file'),
    (2,'Go PSQL Drivers','jackc','pgx/v5/pgxpool','Not Found','Not Found','Not Found','Go module to load environment variables from a file');